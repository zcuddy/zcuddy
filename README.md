<div align="center">
    <h1>Hi there, I'm 
        <a href="https://www.zack-cuddy.com/" target="_blank">Zack Cuddy</a> 
        <img src="https://gitlab.com/zcuddy/zcuddy/-/raw/main/assets/Hi.gif" height="32" />
    </h1>
</div>

Hello everyone, my name is Zack Cuddy and I am a Staff Frontend Engineer here at GitLab!  I support many of the groups in the <a href="https://about.gitlab.com/direction/saas-platforms/tenant-scale/" target="_blank">Tenant Scale Section</a> but my :heart: belongs to <a href="https://about.gitlab.com/direction/core_platform/#geo" target="_blank">Geo</a>.

## :eye: My Vision

Vision Boards are a powerful tool to help envision what you want to spend your energy on each day.  Below is my personal board and offers a high level glance at what I value the most in my life.  I use this board to check in to ensure I am spending my energy on the things that matter most to me!

<div align="center">
    <img src="https://gitlab.com/zcuddy/zcuddy/-/raw/main/assets/Vision_Board.jpg" width="80%"/>
</div>

## :key: Key Facts

- Born/Raised/Reside: St. Louis, Missouri USA
- GitLab Tenure: 5 years
- Engineering Experience: 11 years
- Husband to Kelly and Father to Theodore and Finley
- Dog Dad to Maxwell and Nora
- Cat Dad to Luna

## :tada: Interests/Hobbies

- Reading financial education books :book:
- Stock market :chart_with_upwards_trend: 
- Real estate :house:
- Playing and watching basketball :basketball:
- Taking care of my yard and pool :sunny:
- Playing video games :video_game:
- Playing board games :game_die:
- Weight lifting :muscle:

## :heart: Favorites

- Color: <a href="https://www.colorhexa.com/3498db" target="_blank">#3498DB</a>
- Book: <a href="https://www.goodreads.com/en/book/show/42071647-the-wealthy-gardener" target="_blank">The Wealthy Gardener</a>
- Movie: <a href="https://letterboxd.com/film/good-will-hunting/" target="_blank">Good Will Hunting</a>
- TV Show: <a href="https://www.imdb.com/title/tt0386676/" target="_blank">The Office</a>
- Baseball Team: <a href="https://www.mlb.com/cardinals" target="_blank">St. Louis Cardinals</a>
- Basketball Team: <a href="https://www.nba.com/lakers" target="_blank">Los Angeles Lakers</a>
- Food: <a href="https://www.allrecipes.com/recipe/223042/chicken-parmesan/" target="_blank">Chicken Parmesan</a>
- Candy: <a href="https://www.hersheyland.com/heath" target="_blank">Heath</a>

## :crystal_ball: Misc

- I deleted all social media (yes even Reddit :sweat_smile:) in 2021 to pursue a more hands-off lifestyle and haven't looked back
- My virtual bookshelf: https://www.goodreads.com/review/list/149214966
- I practice and advocate for <a href="https://www.psychologytoday.com/us/therapy-types/internal-family-systems-therapy" target="_blank">Internal Family Systems Therapy</a>
- I ran a profitable subscription based sports/trading card investment company during the height of the COVID pandemic in 2020-2021
- Email Me! zcuddy23@gmail.com

